**Option 1**

1. Click on **Bindtuning**, and then **Edit Web parts** - the edit panel will appear from the left side of the web part;
2. Mouse hover the Marker you want to delete, and click the **trash** icon to delete the Marker;
3. A message will appear requesting your confirmation. Click **Ok** and the web part will refresh and the Marker will be removed.

![delete-map1](../../images/classic/03.delete_map1.gif)

___________
**Option 2**


1. Click on **Bindtuning**, and then **Edit Web parts** - the edit panel will appear from the left side of the web part;
2. On the web part sidebar and click the **Manage Maps** icon;
3. The list of Marker will appear. Click the **trash** icon to delete the Marker;
4. A message will appear requesting your confirmation. Click **Ok** and the web part will refresh and the Marker will be removed.	

![delete-map2](../../images/classic/03.delete_map2.gif)