**Option 1**

1. Click on **Bindtuning**, and then **Edit Web parts** - the edit panel will appear from the left side of the web part;
2. Mouse hover the Marker you want to edit, and click on the ✏️ (pencil) icon;

	![edit-map.gif](../../images/classic/04.edit_map1.gif)

3. You can check what you can edit in each section on the [Marker Location](../../global/location);
4. Done editing? Click on **Save Changes** to save your settings. You can also preview the Map on the page before saving it, by clicking on the **Preview** button.

	![save_buttons.png](../../images/classic/16.save.png)

___________
**Option 2**

1. Click on **Bindtuning**, and then **Edit Web parts** - the edit panel will appear from the left side of the web part;
2. On the web part Marker and click the **Manage Maps** icon;
3. The list of Marker will appear. Click the **Edit** icon to edit the Marker;
4. You can check what you can edit in each section on the [Marker Location](../../global/location);

	![edit-map.gif](../../images/classic/04.edit_map2.gif)

5. Done editing? Click on **Save Changes** to save your settings. You can also preview the Map on the page before saving it, by clicking on the **Preview** button.