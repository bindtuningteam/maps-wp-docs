1. Open the page where you've added the web part; 
2. Click on **Bindtuning**, and then **Edit Web parts** - the edit panel will appear from the left side of the web part;
3. Click on the ➕ (plus) icon to add a new Marker;

	![add_marker.gif](../../images/classic/02.add_marker.gif)

4. Fill out the form that pops up... You can check what you need to do in each section on the [Marker Location](../../global/location);

5. After setting everything up, click on **Save** or **Save and Create Another** in case you want create more markers with similar configuration. You can also preview the Map on the page before saving it, by clicking on the **Preview** button.

	![save](../../images/classic/16.save.png)
