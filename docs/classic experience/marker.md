The Maps Web Part supports both the creation of a new list, directly through its interface, as well as the connection to a pre-existing list.
___
### Create a list

This list is what you will use to store your markers.

1. Click on **Settings** ⚙️ and then on **Add an app**;

	![maps_configuration_11.png](https://bitbucket.org/repo/jgEoyap/images/3024319161-maps_configuration_11.png)

2. On the search box type **Maps**;
3. Look for the Maps list and click to open;
4. Name the list and click on **Create**;

	![maps_configuration_6.png](https://bitbucket.org/repo/jgEoyap/images/562165820-maps_configuration_6.png)

5. You've created the list that will contain all the specific fields from this web part.
6. Open the list that you have created, and copy the list URL, you will need it to connect the list and webpart.

	![maps_configuration_7.png](https://bitbucket.org/repo/jgEoyap/images/1043057723-maps_configuration_7.png)

___
### Connect to a pre-existing List

The Maps Web Part also allows for the mapping of fields in a pre-existing list to display the information in the Web Part. 

To connect to a pre-existing list, follow the steps below: 

1. Edit the **Maps** Web Part; 

1. Under **Marker Options**, select **Manage Lists**; 

1. Click the button **Add list**; 

1. Proceed to input both the **SharePoint site** and corresponding **list** you wish to connect to; 

1. After selecting the list, map the corresponding list columns to the specified field. 

	![map-fields.png](../images/modern/map-fields.png)

	<p class="alert alert-warning">Note that, in order for the mapping to properly work, you'll have to add columns for both <b>Latitude</b> and <b>Longitude</b> to your pre-existing list.</p>

1. Save the configuration. 

This mapping functionality will work the same way, regardless of **Map Provider**, although **Google Maps** offering a few more capabilities.

Since **Google Maps** offers *geocoding* functionalities, both the **Latitude** and **Longitude** columns, previously created, can be left empty, as the coordinates will be, automatically, extracted from the provided **Address** column.

<p class="alert alert-warning">For the <b>Open Street Maps</b> provider, both the <b>Latitude</b> and <b>Longitude</b> columns will have to be manually filled, as it does not offer <em>geocoding</em> functionalities.</p>

___
### List URL 

![markeoptions.png](https://bitbucket.org/repo/jgEoyap/images/1687206012-markeoptions.png)

Paste the URL of the SharePoint list you created on step above.

Without the **List URL**, the web part markers will not be visible.

<p class="alert alert-warning">Use relative paths for this field. So instead of using an URL like <b>https://company.sharepoint.com/sites/Home/Lists/Markers</b>, you should use something like <b>sites/Home/Lists/Markers</b>.</p>


___
### Map Center

Here you can set the location where the map will be centered when it first loads. If you are using **Google Maps**, you can use an address or coordinates, but if you are using **Open Street Maps** only the coordinates option is available.

To check the coordinates go to <a href="https://www.openstreetmap.org/" target="_blank">Open Street Maps</a> and search for the City/State. Select the desired address and at your's URL, you'll have the coordinates that you can add to the **Map Center**. 

![mapcenter](../images/classic/07.mapcenter.png)

<p class="alert alert-success">Coordinates are accepted in <b>decimal degrees</b> format. (Example: 41.368562,-8.7526527). If the value provided is invalid or if you leave this option blank, the web part will use the coordinates (0.00, 0.00).</p>

______
### Map Heigth

The Map Height allows you to adjust the height you want for your map. By default, it’s set to 500px.

___
### Zoom

Here you can set the amount of zoom that will be used when the map first loads. The higher the value, the closer the map will be to the ground. **Maximum** value is 18. The **default** value is 5.