![mapsproviders.gif](https://bitbucket.org/repo/jgEoyap/images/2400339961-mapsproviders.gif)

### Map Provider

The Maps web part offers the option to choose wich Map Provider you want to use with your maps. You have two Maps Providers available:

- [FREE] **Open Street Maps**. 
- [FREE FOR PUBLIC FACING] Or, **Google Maps**. To get an API key, please follow the steps in this link: <a href="https://developers.google.com/maps/documentation/javascript/get-api-key" target="_blank">https://developers.google.com/maps/documentation/javascript/get-api-key</a>. 

___
<p style="background-color:#FEB4AE; border-radius:15px; text-align: center; color:black;"> Warning: Google changed this method, check link above </p>

### How to get an API KEY for Google Maps

1. Go to <a href="https://console.developers.google.com/project/" target="_blank">https://console.developers.google.com/project/</a>;
2. Login to your Google account. If you don’t have one, create one by clicking **Create Account**;
3. Open the **Project** menu at the top and click on **Create a New Project**;

![maps_configuration_1.png](https://bitbucket.org/repo/jgEoyap/images/1395605713-maps_configuration_1.png)

 <p class="alert alert-success">If you already have a project you would like to use, move on to <b>step 5</b>.</p>

4. Pick a name for the project, check or uncheck the options that appear and click **Create**.
    
 <p class="alert alert-success">The page should refresh automatically.</p>

5. Inside the Google APIs list, on the search bar, type **Google Maps JavaScript API** and click on the first results that show up;
7. Click on **Activate the API** and then **Create Credentials**;
8. There are 2 dropdown inputs with Google Maps JavaScript API and Web Browser (JavaScript) selected. Click **Continue**;
9. Give a name to the key;
10. For the **Key restriction**, choose **HTTP referrers (web sites)** and type the URL of where you want to add the web part;

	![maps_configuration_2.png](https://bitbucket.org/repo/jgEoyap/images/1219658641-maps_configuration_2.png)

11. Click **Save**;

	<p class="alert alert-success">If you want to have the map available for all the pages inside the site collection, add to the end of the URL a "/*". e.g.: https://contoso.sharepoint.com/sites/*</p>

11. To finish it off, copy the key provided - you will need it for setting the **Google Maps API Key** option of your web part properties.

	![maps_configuration_4.png](https://bitbucket.org/repo/jgEoyap/images/3336925421-maps_configuration_4.png)

	<p class="alert alert-success">You can also click <strong>Get API Key</strong> and copy the value from the page.</p>