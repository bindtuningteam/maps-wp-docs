![marker_location](../images/classic/18.marker_location.png)

Set marker position on the map either by using an address or coordinates. You can choose between use Address or Coordinates if you are using **Google Maps**. If you are using **Open Street Maps** only the Coordinates option is available.

To check the coordinates go to <a href="https://www.openstreetmap.org/" target="_blank">Open Street Maps</a> and search for the City/State. Select the desired address and at your's URL, you'll have the coordinates that you can add to the **Map Center**. Use the structure described below. 

<p class="alert alert-success">Coordinates are accepted in <b>decimal degrees</b> format. (Example: 41.368562,-8.7526527). If the value provided is invalid or if you leave this option blank, the web part will use the coordinates (0.00, 0.00).</p>

Clicking on next will open the next section of the **Marker Captions**. 