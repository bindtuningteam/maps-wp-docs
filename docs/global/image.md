When you select the option to be an image it will allow you to:

![custom_marker](../images/classic/13.custom_marker.png)

- **Insert an URL** of the image location directly to the text area;
- **Browse on your SharePoint site** and pick a picture. If you click on **Browse** that will open a panel where you can select an image from your SharePoint libraries. 

![image_picker](../images/classic/14.image_picker.png)

### Upload image

You can also upload your image for the select library by selecting the icon at the top to **Upload File** or by **Dragging the image** for the area. Note this will upload the image directly to Library that you've selected. 