![marker_settings](../images/classic/17.marker_settings.png)

### Marker Tooltip

Here you can set the text that appears when you move the mouse cursor over the marker.

___
### Custom Marker

This setting allows you to set custom marker icons. To do that place the URL of the custom marker image you want to use. You can also use the image picker to select an image from your SharePoint site. For more information about this visti the <a href="./location" target="_blank">next link</a>.

<p class="alert alert-info">If you don't use a custom marker, the Google Maps marker icon is displayed.</p>

___
### Marker Status

If you set this option to **Active**, the marker will be displayed on the map. Set to **Inactive** if you want to hide the marker.