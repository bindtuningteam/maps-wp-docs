![marker_caption](../images/classic/19.marker_caption.png)

Markers can display captions, video, icons or images that appear whenever you click on the marker.

You can setup the caption content using the quick format buttons above the text area, on the [next link](./format) you can find more information. All these options are instantly converted to Markdown and will appear in the text box. You can also edit the Markdown directly on the text box.

If you don't know much about Markdown read more about it on our <a href="https://support.bind.pt/hc/en-us/articles/213784643" target="_blank">Markdown syntax in BindTuning</a> article.

Clicking on next will open the next section of the **Marker Settings**. 