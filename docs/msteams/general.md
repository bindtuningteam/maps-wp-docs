![options](../images/modern/07.options.png)

On the Web Part Properties panel, you've multiple options which you can edit for diferent configuration of the Web Part.

- [Create New BT Maps List](./createlist.md)
- [Provider Options](./provider.md)
- [Marker Options](./marker.md)
- [Map Center Options](./map.md)
- [Map Size](./size.md)
- [Advanced Options](./advanced.md)
- [Web Part Messages](./message.md)