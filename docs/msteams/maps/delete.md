1. Open the Team on **Teams panel** that you intend to remove the Web Part; 
2. Click on the **Settings** button.

	![settings_delete.png](../../images/msteams/setting_edit.png)

2. On the web part sidebar and click the **Manage Maps** icon;

    ![manage_maps.png](../../images/msteams/manage_maps.png)

3. The list of Marker will appear. Click the **trash** icon to delete the Marker;

	![delete_map.png](../../images/msteams/delete_map.png)

4. A message will appear requesting your confirmation. Click **Ok** and the web part will refresh and the Marker will be removed.