1. Open the Team on **Teams panel** that you intend to edit the Web Part; 
2. Click on the **Settings** button.

	![settings_delete.png](../../images/msteams/setting_edit.png)
2. On the web part sidebar and click the **Manage Maps** icon;

	![manage_maps.png](../../images/msteams/manage_maps.png)

3. The list of Maps will appear. Click the **pencil** icon to edit the Map Marker;

	![edit_map.png](../../images/msteams/edit_map.png)

4. You can check what you can edit in each section on the [Marker Location](../../global/location);
5. Done editing? Click on **Save Changes** to save your settings. You can also preview the Map on the page before saving it, by clicking on the **Preview** button.