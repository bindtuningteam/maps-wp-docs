<p class="alert alert-info">Note: You need to have a list connect to BindTuning Maps to see this options.</p>

1. Open the Team on **Teams panel** that you intend to add the Web Part; 
2. Click on the **Settings** button. 

	![settings_delete.png](../../images/msteams/setting_edit.png)

2. Mouse hover the Web Part and click on the **[+]** button to add a new **Maps Marker**;
3. Fill out the form that pops up. You can check out what you need to do in each setting in the [Marker Location](../../global/location);

4. After setting everything up, click on **Save** or **Save and Create Another** in case you want create more markers with similar configuration. You can also preview the Map on the page before saving it, by clicking on the **Preview** button.

	![Add_new_alert_teams.gif](../../images/msteams/add_new_maps_teams.gif)