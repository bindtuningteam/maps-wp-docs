1. On the Tab click on the dropdown and then **Settings**. If is the first time adding the **Maps**, you can skip this process. 

    ![setting_edit.png](../images/msteams/setting_edit.png)

2. On The panel select the **Create New BT Maps List**
3. Type the name of the list and hit **Create the List**;    
3. Configure the web part according to the settings described in the **[Web Part Properties](./general.md)**;

4. The properties are saved automatically, so when you're done, simply **Publish** the page and the content will be saved.