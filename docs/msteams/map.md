![map_settings](../images/modern/12.map_settings.png)

### Zoom

Here you can set the amount of zoom that will be used when the map first loads. The higher the value, the closer the map will be to the ground. **Maximum** value is 18. The **default** value is 5.

___
### Map Center

Here you can set the location where the map will be centered when it first loads. If you are using **Google Maps**, you can use an address or coordinates, but if you are using **Open Street Maps** only the coordinates option is available.

To check the coordinates go to <a href="https://www.openstreetmap.org/" target="_blank">Open Street Maps</a> and search for the City/State. Select the desired address and at your's URL, you'll have the coordinates that you can add to the **Map Center**. 

![mapcenter](../images/classic/07.mapcenter.png)

<p class="alert alert-success">Coordinates are accepted in <b>decimal degrees</b> format. (Example: 41.368562,-8.7526527). If the value provided is invalid or if you leave this option blank, the web part will use the coordinates (0.00, 0.00).</p>