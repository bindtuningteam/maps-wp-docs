![mapheigth](../images/modern/09.mapheigth.png)

### Map Heigth

The Map Height allows you to adjust the height you want for your map. By default, it’s set to 500px.