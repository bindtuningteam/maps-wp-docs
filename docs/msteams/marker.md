
If you have already configured a BindTuning Maps list that you intend to display, you need to paste the **list URL** that you have copied when you finished to create the list. This list will be stored on a SharePoint site.

![marker](../images/modern/13.marker.png)

<p class="alert alert-info">We recommend you use relative paths in this field. So instead of using a URL like  https://company.sharepoint.com/sites/Home/Lists/Maps you should use something like <b>/sites/Home/Lists/Maps</b>. This will ensure that the web part will work regardless of how you’re accessing the site. </p>

You can create a BindTuning Maps list using the panel, for more information about this visit the [next link](./createlist).