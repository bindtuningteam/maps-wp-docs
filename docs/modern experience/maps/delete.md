**Option 1**

1. Click on the **Edit** button;

![edit-page](../../images/modern/01.edit.modern.png)

2. Mouse hover the Marker you want to delete, and click on the markert, then on **trash** icon to delete the Marker;
3. A message will appear requesting your confirmation. Click **Ok** and the web part will refresh and the Marker will be removed.

![delete Map](../../images/modern/02.delete_map1.gif)
__________
**Option 2**

1. Click on the **Edit** button;

![edit-page](../../images/modern/01.edit.modern.png)

2. On the web part sidebar and click the **Manage Maps** icon;
3. The list of Marker will appear. Click the **trash** icon to delete the Marker;
4. A message will appear requesting your confirmation. Click **Ok** and the web part will refresh and the Marker will be removed.

![delete Map](../../images/modern/02.delete_map2.gif)