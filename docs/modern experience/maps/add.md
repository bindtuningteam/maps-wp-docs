1. Click on the **Edit** button;

![edit-page](../../images/modern/01.edit.modern.png)

2. Mouse hover the Web Part and click on the **[+]** button to add a new **Maps Marker**;

	![add.marker](../../images/modern/01.add.marker.gif)

3. Fill out the form that pops up. You can check out what you need to do in each setting in the [Marker Location](../../global/location);

4. After setting everything up, click on **Save** or **Save and Create Another** in case you want create more markers with similar configuration. You can also preview the Map on the page before saving it, by clicking on the **Preview** button.