**Option 1**

1. Click on the **Edit** button;

![edit-page](../../images/modern/01.edit.modern.png)

2. Mouse over the Map marker you intend to edit and click, then click on the **pencil** icon to edit the Map:
3. You can check what you can edit in each section on the [Marker Location](../../global/location);
4. Done editing? Click on **Save Changes** to save your settings. You can also preview the Map on the page before saving it, by clicking on the **Preview** button.

![edit-map.gif](../../images/modern/03.edit_map1.gif)
__________
**Option 2**

1. Click on the **Edit** button;

![edit-page](../../images/modern/01.edit.modern.png)

2. On the web part sidebar and click the **Manage Maps** icon;
3. The list of Maps will appear. Click the **pencil** icon to edit the Map Marker;
4. You can check what you can edit in each section on the [Marker Location](../../global/location);

	![edit-map.gif](../../images/modern/03.edit_map2.gif)

5. Done editing? Click on **Save Changes** to save your settings. You can also preview the Map on the page before saving it, by clicking on the **Preview** button.