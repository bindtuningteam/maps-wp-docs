![webpartapperence](../images/modern/06.webpartapperence.png)

### Theme Colors on the Title Bar

Enabling this option will make it so the web part's title bar inherits the background color from your theme or customization on the page. 
This option should help in maintaining branding consistency with native SharePoint web parts.