The Maps Web Part supports both the creation of a new list, directly through its interface, as well as the connection to a pre-existing list.
___
### Create List 
![06.options](../images/modern/08.createlist.png)

Type the name of the list that you want to create and click **Create the List**;

The list will be created on your current Site and connected with the Web Part;

If you intend to edit the list connection go to the [next link](./marker.md)

___
### Connect to a pre-existing List

The Maps Web Part also allows for the mapping of fields in a pre-existing list to display the information in the Web Part. 

To connect to a pre-existing list, follow the steps below: 

1. Edit the **Maps** Web Part; 

1. Under **Marker Options**, select **Manage Lists**; 

1. Click the button **Add list**; 

1. Proceed to input both the **SharePoint site** and corresponding **list** you wish to connect to; 

1. After selecting the list, map the corresponding list columns to the specified field. 

	![map-fields.png](../images/modern/map-fields.png)

	<p class="alert alert-warning">Note that, in order for the mapping to properly work, you'll have to add columns for both <b>Latitude</b> and <b>Longitude</b> to your pre-existing list.</p>

1. Save the configuration. 

This mapping functionality will work the same way, regardless of **Map Provider**, although **Google Maps** offering a few more capabilities.

Since **Google Maps** offers *geocoding* functionalities, both the **Latitude** and **Longitude** columns, previously created, can be left empty, as the coordinates will be, automatically, extracted from the provided **Address** column.

<p class="alert alert-warning">For the <b>Open Street Maps</b> provider, both the <b>Latitude</b> and <b>Longitude</b> columns will have to be manually filled, as it does not offer <em>geocoding</em> functionalities.</p>