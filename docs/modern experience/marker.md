![marker](../images/modern/13.marker.png)

Paste the URL of the SharePoint list you created on step above.

Without the **List URL**, the web part markers will not be visible.

<p class="alert alert-warning">Use relative paths for this field. So instead of using an URL like <b>https://company.sharepoint.com/sites/Home/Lists/Markers</b>, you should use something like <b>sites/Home/Lists/Markers</b>.</p>

You can create a BindTuning Maps list using the panel, for more information about this visit the [next link](./createlist).